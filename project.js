/*
Project by: Reece Appling

//___________________________________________________________________________________________________________________________________________________________________________________________________
//LIBRARIES UTILIZED  name, description, and link.

Decimal							Allows for more accurate math									https://github.com/MikeMcl/decimal.js/
*/const Decimal = require('decimal.js');/*
neutrium Quantity				Quantity branch that allows units to be connected to numbers	https://github.com/neutrium/quantity/blob/master/README.md
*/const Quantity = require("@neutrium/quantity").Quantity;/*
Filesystem						library required to use filesystem properties					
*/const fs = require('fs');

//_________________________________________________________________________________________________________________________________________________________________________________________________
//FUNCTION PROTOTYPING

Quantity.prototype.display = function(){return this.toNumber()+" "+this._units;};//function to display a string of human-readable Quantity metrics, (EX: "183 in")
Quantity.prototype.show = function(){return this.display;};//function synonymous to the above
Quantity.prototype.toNumber = function(){return this.scalar.toNumber();};//create new Quantity function to convert to integer

//___________________________________________________________________________________________________________________________________________________________________________________________________
//LOCAL IMPORTS

const units = require('./units');//empirical and metric units and their corresponding variables
const pyInterface = require('./pythonInterface');//interface to run python scripts from a node app
const userSettings = require('./userSettings.json');//object for local user settings
//_________________________________________________________________________________________________________________________________________________________________________________________________
//Constant Declarations

//local program/directory constants
const pythonDirectory = userSettings.pythonDirectory;//sets local Python directory to that required by usersettings
const thisDirectory = __dirname;//directory this project is housed in
const pythonScript = 'steamTables.py';//name of steamtables script
const steamyPython = pyInterface.setup(pythonDirectory,thisDirectory);//setup python steamTables Interface


//physical constants
const gc = 4.17*(Math.pow(10,8));//gravitational constant in ft/hr2


//Provided project constants
const CPRmax = 1.2; //Maximum CPR allowed
const meltmax = 1.2; //Maximum Qmelt/Q allowed, where Qmelt is the Q at which Tmelt is met at the highest fuel centerline temp
const Tmelt = new Quantity('4980 degF');//Fuel melting temperature
const Q = new Quantity('3926 MW');//Core thermal output
const gamma = 0.974;//Fraction of energy deposited in fuel
const P = new Quantity('1040 psi');//System pressure
const TFeed = new Quantity('420 degF');//Feed temperature
const Fq = 3.2;//Power peaking factor
const Fz = 1.4;//Axial peaking factor
const H = new Quantity('176 in').to('ft');//Fuel height (canned)
const Do = new Quantity('0.4039 in').to('ft');//Outer rod diameter
const Ro = Do.div(2);//Outer rod radius
const Di = new Quantity('0.3441 in').to('ft');//Inner rod diameter
const Ri = Di.div(2);//Inner rod radius
const Dp = new Quantity('0.3386 in').to('ft');//Fuel pellet diameter
const Rf = Dp.div(2);//Fuel pellet radius
const S = new Quantity('0.5098 in').to('ft');//Rod pitch (square lattice)
const kc = new Quantity('9.6 Btu/hr ft degF');//Clad thermal conductivity
const HG = new Quantity('1000 Btu/hr ft^2 degF');//Gap conductance
const CanDim = new Quantity('5.52 in').to('ft');//Can dimensions
const nAssemb = 872;//Number of assemblies in core
const fuelRodsPerAssemb = 92;//Fuel rods per assembly
const waterRodsPerAssemb = 8;//Water rods per assembly
const nSpacers = 8;//Number of spacer grids in core
const Kgrid = 0.5;//Grid loss coefficient
const KcoreIn = 1.5;//Core inlet loss coefficient (includes acceleration)
const KcoreOut = 1;//Core exit loss coefficient
const Kdowncomer = 2;//Downcomer loss coefficient
const Dvessel = new Quantity('280 in').to('ft');//Vessel Diameter
const Dbarrel = new Quantity('184 in').to('ft');//Core Barrel Diameter 
const Dchimney = Dbarrel;//Chimney Diameter 
const Hchimney = new Quantity('12 ft');//Chimney Height
const Hdowncomer = Hchimney.add(H).to('ft');


//constants found by hand (and location of work if necessary)
const hf = new Quantity('548.746 '+units.emp.h);//Saturated liquid enthalpy @ system pressure
const hg = new Quantity('1191.05 '+units.emp.h);//Saturated gas enthalpy @ system pressure
const hFeed = new Quantity('397.536 '+units.emp.h);//feed enthalpy, from P and TFeed
const hfg = hg.sub(hf);//hfg @ system pressure = hg - hf
const Tsat = new Quantity('549.432 '+units.emp.t);//Saturation temperature of water @ P
const Tin = new Quantity('536.639 '+units.emp.t);;//Inlet temperature (due to low subcooling)							//-----------------------------------------------------------------------
const hin = new Quantity('548.186 '+units.emp.h);;//inlet enthalpy, hf (because negligible inlet subcooling assumption)	//---------------------------------------------
const hsub = hf.sub(hin);//enthalpy subcooled, saturated fluid enthalpy - inlet enthalpy
const Pc = new Quantity('3208.2 psi');//Critical pressure for water, from steam tables
const rhoG = new Quantity('2.34259 '+units.emp.rho);//Saturated liquid density @ system pressure
const rhoF = new Quantity('45.9901 '+units.emp.rho);//Saturated gas density @ system pressure
const muF = new Quantity('0.219226 '+units.emp.mu);//Saturated liquid viscosity @ system pressure
const muG = new Quantity('0.0460478 '+units.emp.mu);//Saturated gas viscosity @ system pressure



//Pre-naming variables that have not yet been calculated
var xMax;//Used to find extrapolation length
var lambda;//Extrapolation length
var He;//H + (2*lambda)



//_________________________________________________________________________________________________________________________________________________________________________________________________
//FUNCTION DECLARATIONS

//integral shape function renamed as x that is useful for finding lambda among other things
function xConst(z){
	if(typeof z == 'object'){return (lambda.to('ft').toNumber()-z.to('ft').toNumber()+H.to('ft').toNumber())*(Math.PI)/(He.to('ft').toNumber());}
	else{return (lambda.to('ft').toNumber()-(z)+H.to('ft').toNumber())*(Math.PI)/(He.to('ft').toNumber());}
}

//heat flux integral (and synonymous functions) from 0 to z, no outer terms (He/Do/gamma/m)
//--------------------------------------------------------Re-check and MAKE SURE ALL UNITS ARE CORRECT-----------------------------------
function integralTo(z){//SHOW WHERE THIS COMES FROM --------------------------------------------------------------------
	let zConst = xConst(z);
	let constHB = ((lambda.to('ft').toNumber()+(H.to('ft').toNumber()))*Math.PI/He.to('ft').toNumber());
	return ((zConst*Math.cos(zConst))-(constHB*Math.cos(constHB))-Math.sin(zConst)+Math.sin(constHB)); //FROM HW 4.2.c==============================================
}function integral(z){return integralTo(z);}function integrate(z){return integralTo(z);}function integrateTo(z){return integralTo(z);}//setting up synonymous functions

//----------------------------------------------
//Calculating enthalpy along core, z in FT
function enthalpy(z,mChanl,qNot){return hin.to('Btu/lb').add(qNot.mul(Do.mul(He)).mul(integralTo(z)).div(mChanl.to('lb/hr')).div(gamma));}//cite this equation, hw4.2.c=============================

//Calculating fluid quality
function quality(enthalp){
	let h=enthalp;
	if(typeof h == 'object'){h=h.toNumber();}
	let hCheck = (h-hf.toNumber())/hfg.toNumber();
	if(hCheck<0){return 0;}
	else{
		if(hCheck>1){return 1;}
		else{return hCheck;}
	}
}

function voidFrac(rho){return((rho-rhoF.toNumber())/(rhoG.toNumber()-rhoF.toNumber()));}

//Calculating Martinelli Parameter
function martinelli(x){return Math.pow(muF.toNumber()/muG.toNumber(),0.2)*Math.pow((1-x)/x,1.8)*(rhoG.toNumber()/rhoF.toNumber());}

//Calculating two-phase friction multiplier
function twoPhase(x){return ((1+(20/martinelli(x))+(1/Math.pow(martinelli(x),2)))*Math.pow(1-x,1.8));}

//Calculating, saving, and returning LCrit
function Lcrit(zn,zcrit){LCrit = zcrit.sub(zn);return LCrit;}//currently not used, most likely won't need it --------------------------------------------------

//function (a) used in critical boiling length correlation numerator
//Gx MUST be integer in lb/ft^2 hr ------------------------------------------------------------------------
//MUST output an integer ----------------------------------------------------------------------------------------------------------
function xCritA(Gx){//ALSO A FUNCTION OF PRESSURE, BUT P=CONST--------------------------------------------------------------------
	var aTemp;
	if(Gx.to('kg/m^2 s').toNumber()>(Math.pow(1-(P.to('MPa').toNumber()/(Pc.to('MPa').toNumber())),3))){
		aTemp = ((1-(P.to('MPa').toNumber()/(Pc.to('MPa').toNumber())))/Math.pow(Gx.to('kg/m^2 s').toNumber()/1000,1/3));
	}
	else{aTemp = (1/(1+(1.481*(10^(0-4))*Math.pow(1-(P.to('MPa').div(Pc.to('MPa')).toNumber()),0-3)*(Gx.to('kg/m^2 s').toNumber()))));}
	return aTemp*(Do.to('ft').toNumber())/(De.to('ft').toNumber());
}

//function (b) used in critical boiling length correlation numerator
//Gx MUST be integer in lb/ft^2 hr ------------------------------------------------------------------------
function xCritB(Gx){//ALSO A FUNCTION OF PRESSURE, BUT P=CONST--------------------------------------------------------------------
	return new Quantity((Gx.to('kg/m^2 s').scalar.mul(0.199).mul(Do.to('m').scalar.pow(1.4)).mul(Pc.to('MPa').scalar.div(P.to('MPa').scalar.sub(1)).pow(0.4)).toNumber())+" m");
}

//Critical Boiling Length Correlation
function xCrit(Gx,critLength){//Gx and critLength MUST be a Quantity
	let Lcr = critLength.to('m').scalar;let Go = Gx.to('kg/m^2 s').toNumber();//currently not used, most likely won't need it --------------------------------------------------
	return Do.mul(Lcr.mul(xCritA(Go)).div(Lcr.add(xCritB(Go)))).div(De);
}

//Calculating heat flux given q''o and z
function heatFlux(z,qNot){return qNot.mul(xConst(z)).mul(Math.sin(xConst(z)));}

//Calculating temp of a fluid in the core based on z and qNot
function fluidTemp(z,mChanl,qNot){
	return new Promise((resolve,reject)=>{
		let hPy = enthalpy(z,mChanl,qNot).to(units.met.h);
		steamyPython.run([pythonScript,P.to('MPa').toNumber(),hPy.to('kJ/kg').toNumber(),"PH"]).then((pyOut)=>{
			resolve(new Quantity((((pyOut[0]-273.15)*(9/5))+32)+" degF"));
		},(pyErr)=>{reject(pyErr);});
	});
	
}

//single phase friction factor
//MAKE SURE THIS WILL BE CORRECT!!!!-------------------------------------------------------------------------------------------------------------------------
function frictionFactor(re){
	if(re<30000){return 0.3164*Math.pow(re,0-0.25);}
	else{return 0.184*Math.pow(re,0-0.2);}
}


//check results against criteria
function checkResults(CPR,Qmelt){ //currently not used, most likely will integrate this functionality elsewhere --------------------------------------------------
	let checkVar = true;
	if(CPR>CPRmax){console.log("CPR = "+CPR+", OK! (>"+CPRmax+")");}
	else{console.log("CPR = "+CPR+",CPR IS TOO LOW! (CPR must be > "+CPRmax+")");checkVar=false;}
	if(Qmelt.div(Q).toNumber()>=1.2){console.log("Qmelt/Q = "+(Qmelt.div(Q).toNumber())+", OK! (>"+meltmax+")");}
	else{console.log("Qmelt/Q = "+(Qmelt.div(Q).toNumber())+",Qmelt/Q IS TOO LOW! (must be >= "+meltmax+")")}
}

function bisectionMethod(funcOne,funcTwo,bounds,acc){
	return new Promise((accept,reject)=>{
		let newBounds=bounds;let lastxn=bounds[0];
		let compoundFunction = function(x){return (funcOne(x)-funcTwo(x));};
		while(true){
			let midPt = (newBounds[0]+newBounds[1])/2;
			if(compoundFunction(midPt)==0||(Math.abs(midPt-newBounds[0])<acc&&Math.abs(midPt-newBounds[1])<acc)){accept(midPt);break;}
			else{
				lastxn=midPt;
				if(Math.sign(compoundFunction(newBounds[0]))==Math.sign(compoundFunction(midPt))){newBounds[0]=midPt;}
				else{if(Math.sign(compoundFunction(newBounds[1]))==Math.sign(compoundFunction(midPt))){newBounds[1]=midPt;}
					else{reject('ERROR! BISECTION ITERATION YIELDED NaN!');break;}
				}
			}
		}
	});
}

function approxMax(func,bounds){
	let maxF = 0;
	for(var i=0;i<100;i++){if(func(bounds[0]+(i*(bounds[1]-bounds[0])/100))>func(maxF)){maxF = i;}}
	return [bounds[0]+((maxF-1)*(bounds[1]-bounds[0])/100),bounds[0]+((maxF+1)*(bounds[1]-bounds[0])/100)];
}

function findSigns(func,bounds){
	let signs=[0,0];
	for(var i=0;i<100;i++){
		let toCheck=func(bounds[0]+(i*(bounds[1]-bounds[0])/100));
		if(toCheck>0){signs[1]=bounds[0]+(i*(bounds[1]-bounds[0])/100);}
		if(toCheck<0){signs[0]=bounds[0]+(i*(bounds[1]-bounds[0])/100);}
		if(signs[0]!=0&&signs[1]!=0){return signs;break;}
	}return signs;
}





//False position method of finding roots in transcendental equations
function falsePositionMethod(funcOne,funcTwo,boundsOrig,acc){
	return new Promise((accept,reject)=>{
		let loops = 10;//IS 10 OK TO START??????????????????????----------------------------------------------------------
		let compoundFunction = function(x){return (funcOne(x)-funcTwo(x));};
		let newBounds = boundsOrig;
		let lastxn=0;
		while(true){
			let toGo = newBounds;
			let xn = toGo[0]-(((toGo[1]-toGo[0])*compoundFunction(toGo[0]))/(compoundFunction(toGo[1])-compoundFunction(toGo[0])));
			if(Math.abs(xn-lastxn)<acc){accept(xn);break;}
			if(compoundFunction(xn)==0){accept(xn);break;}
			if(Math.sign(compoundFunction(xn))==NaN){reject('ERROR! ITERATION YIELDED NaN!');}
			if(compoundFunction(xn)*compoundFunction(toGo[0])>0){newBounds[0]=xn;}else{newBounds[1]=xn;}
			if(Math.abs(newBounds[0]-newBounds[1])<acc){accept(xn);break;}
			lastxn = xn;
		}
		reject(falsePosition);
	});
}

//Directive function for iterative functions
function iterate(eqOneIn,eqTwoIn,bounds,method,acc){
	let eqOne = eqOneIn; let eqTwo = eqTwoIn;//set temporary function variables
	if(typeof eqOne!="function"){eqOne=function(x){return eqOneIn;};}//creates an arbitrary function if eq1 is a number instead of a function
	if(typeof eqTwo!="function"){eqTwo=function(x){return eqTwoIn;};}//creates an arbitrary function if eq2 is a number instead of a function
	switch(method){
		case "falsePosition":return falsePositionMethod(eqOne,eqTwo,bounds,acc);break;
		case "bisection":return bisectionMethod(eqOne,eqTwo,bounds,acc);break;
		default:console.log("Method "+method+" not recognized for iteration, falling back to bisection method!");return bisectionMethod(eqOne,eqTwo,bounds,acc);
	}
}

//finding the function of channel mass flow that will yield CPR (in the form above), given a core Qcor
function findCPRFunction(Qcor){
	return function(Gcor){//ACCEPTS EMPIRICAL CHANNEL MASS FLOW//EQs from 10/11 in boiling length notes=====================================
		let Gx = new Quantity(Gcor+' kg/m2 s');
		let mCh = Gx.to('kg/m2 s').mul(Ax.to('m2')).to("kg/s");
		let COne = mCh.to("kg/s").mul(hfg.to(units.met.h).mul(xCritA(Gx)).add(hf.to(units.met.h).sub(hin.to(units.met.h)))).div(xCritB(Gx).to('m').div(H.to('m')).add(1)).to("kJ/s").toNumber();
		let CTwo = Qcor.qHotavg.to("kJ/s m^2").mul(H.to('m')).mul(Do.to('m').mul(Math.PI)).to("kJ/s").toNumber();
		let Cout = (COne/CTwo)-CPRmax;
		return Cout;
	}
}

function tInf(z,mChanl,qNot,cp){
	let localEnthalpy = enthalpy(z,mChanl,qNot).toNumber();
	if(localEnthalpy<=hg.toNumber()&&localEnthalpy>=hf.toNumber()){
		return Tsat.toNumber();
	}else{
		return qNot.mul(integralTo(z)).mul(He.mul(Do)).div(mChanl.mul(cp)).add(Tin).toNumber();
	}
}

function incipientBoilingEquation(q,mch,conP,fc){
	return function(z){
		let qo=q;let m=mch;let cp=conP;let hc=fc;
		let toOut2 = Tsat.to('degF').toNumber()-tInf(z,m,qo,cp)-(heatFlux(new Quantity(z+" ft"),qo).to('Btu/hr ft2').toNumber()/hc.toNumber());
		return toOut2;
	};
}

async function incipientBoilingPoint(Qcor,m,cp,hc){
	let toIterate = incipientBoilingEquation(Qcor.qohot,m,cp,hc);
	let incipBounds = findSigns(toIterate,[0,H.to('ft').toNumber()]);
	let toOutputzn = await iterate(toIterate,0,incipBounds,"falsePosition",1/(12*100));
	return toOutputzn;
}

//Tclad in nucleate boiling region, JENS LOTTES
function TcoNB(q){
	return function(z){
		let qo=q;
		let squiggle = Math.exp(4*(P.to('psi').toNumber())/900)/(Math.pow(60,4));
		return Tsat.toNumber()+(Math.pow(squiggle,-1/2)*Math.pow((heatFlux(z,qo).toNumber())/Math.pow(10,6),1/2));
	};
}

//Nucleate boiling heat flux in nucleate boiling region, JENS LOTTES
function qNB(z,qo,mCh,cp,hc){
	let squiggle = Math.exp(4*(P.to('psi').toNumber())/900)/(Math.pow(60,4));
	let megaSquiggle = Math.pow(10,6)*squiggle;
	let wSuperHeat = (TcoNB(qo)(z))-(Tsat.toNumber());
	return Math.pow(wSuperHeat,4)*megaSquiggle;
}

//see above
function qNBfromTcoNB(q,m,conP,fc){
	return function(z){
		let qo=q;let mCh=m;let cp=conP;let hc=fc;
		let squiggle = Math.exp(4*(P.to('psi').toNumber())/900)/(Math.pow(60,4));
		let megaSquiggle = Math.pow(10,6)*squiggle;
		let tconbtemp = TcoNB(qo);
		let wSuperHeat = (tconbtemp(z))-(Tsat.toNumber());
		return Math.pow(wSuperHeat,4)*megaSquiggle;
	};
}

//see above, but looking for FC instead of NB
function qFCNb(q,m,conP,fc){
	return function(z){
		let qo=q;let mCh=m;let cp=conP;let hc=fc;
		let tco = TcoNB(qo);
		return (hc.toNumber()*(tco(z)-tInf(z,mCh,qo,cp)));
	};
}

function mixedBoiling(zz,q,m,conP,fc,QNBZN){//UNTESTED---------------------------------------------------------------------------------------------
	return function(tco){
		let z=z;let qo=q;let mCh=m;let cp=conP;let hc=fc;let qnbzn=QNBZN;
		let qtempFC=hc.toNumber()*(tco-tInf(z,mCh,qo,cp));
		let qtempNorm=heatFlux(z,qo).toNumber();
		let qtempNB=Math.pow(tco-(Tsat.toNumber()),4)*Math.pow(10,6)*Math.exp(4*(P.to('psi').toNumber())/900)/(Math.pow(60,4));
		return qtempNorm-(Math.pow(1+(Math.pow((qtempNB(tco)/qtempFC)*(1-(qnbzn/qtempNB(tco))),2)),1/2)*qtempFC);
	}
}

async function TcMix(zz,q,m,conP,fc,QNBZN){//UNTESTED---------------------------------------------------------------------------------------------
	let z=z;let qo=q;let mCh=m;let cp=conP;let hc=fc;let qnbzn=QNBZN;
	let toItMix = mixedBoiling(zz,q,m,conP,fc,QNBZN);
	let tclmix = await iterate(toItMix,0,findSigns(toItMix,[Tin.toNumber(),Tmelt.toNumber()]),"bisection",0.5);
}

async function findMaxFuelTemp(Qcor,Gcor,isFinal){
	let qo = Qcor.qohot;
	let Gx = new Quantity(Gcor+" kg/m2 s").to('lb/ft2 hr');
	let mCh = Gx.to('lb/ft2 hr').mul(Ax.to('ft2')).to("lb/hr");
	//find avg hot channel temperature, and corresponding state object
	let tHO = await fluidTemp(H.to('ft'),mCh.to("lb/hr"),Qcor.qohot);
	let ThotOut = tHO;
	console.log("Outlet Hot Temp = "+ThotOut.toNumber()+" ------ Sat Temp = "+Tsat.toNumber());
	let Tavg = ThotOut.sub(ThotOut.sub(Tin).div(2));
	let pyLookup = new Promise((accept,reject)=>{
		steamyPython.run([pythonScript,P.to('MPa').toNumber(),(((Tavg.to('F').toNumber()-32)*5/9)+273.15),"PT"]).then((pyOut)=>{
			let TavgFlu = {};
			TavgFlu.cp = new Quantity(pyOut[3]+" "+units.met.cp).to(units.emp.cp);
			TavgFlu.mu = new Quantity(pyOut[4]+" "+units.met.mu).to(units.emp.mu);
			//TavgFlu.k = new Quantity(pyOut[5]+" "+units.met.k).to(units.emp.k);
			TavgFlu.rho = new Quantity(pyOut[5]+" kg/m3").to(units.emp.rho).toNumber();
			TavgFlu.k = new Quantity(pyOut[6]+" "+units.met.k).to(units.emp.k);
			TavgFlu.u = new Quantity(pyOut[7]+" "+units.met.u).to(units.emp.u);
			TavgFlu.t = Tavg;
			accept(TavgFlu);
		},(pyErr)=>{reject(pyErr);});
	});
	let TavgFluid = await pyLookup;
	let cp = TavgFluid.cp;
	//TavgFluid is now an accessable variable
	//find convective heat transfer coefficient (hc)
	let Re = Gx.to('lb/ft^2 hr').mul(De.to('ft')).div(TavgFluid.mu.to('lb/ft hr')).toNumber();//calculating Reynolds number
	let Pr = TavgFluid.cp.mul(TavgFluid.mu).div(TavgFluid.k).toNumber();//calculating prandlt number
	let dittConst = (0.042*(S.to('ft').toNumber()/Do.to('ft').toNumber()))-0.024;//calculating Dittus-Boelter constant for square lattices
	let hc = TavgFluid.k.mul(dittConst).mul(Math.pow(Re,0.8)*Math.pow(Pr,1/3)).div(De);//calculating convective heat transfer coefficient
	//begin finding temperatures and their functions
	let TInfo = {};
	//find zn
	TInfo.zn = await incipientBoilingPoint(Qcor,mCh,TavgFluid.cp,hc);
	let zn=TInfo.zn;
	console.log("Finished, zn = ",TInfo.zn," ft");
	//define mixing function
	TInfo.qNbzn = qNB(TInfo.zn,Qcor.qohot,mCh,TavgFluid.cp);console.log("qnb(zn): "+TInfo.qNbzn);
	let qnbzn = TInfo.qNbzn;
	//find zb
	let qorig = function(z){return heatFlux(z,qo).toNumber();}
	let qfcnb = qFCNb(qo,mCh,cp,hc);//works
	let tconb = TcoNB(qo);//works
	let qnb = qNBfromTcoNB(qo,mCh,cp,hc);//works
	let qmix = function(z){return (qorig(z)*Math.pow(1+Math.pow((qorig(z)/qfcnb(z))*(1-(qnbzn/qorig(z))),2),1/2));};//works
	let nbToIterate = function(z){return qnb(z)-qmix(z)};//works
	let nbBounds = findSigns(nbToIterate,[TInfo.zn,H.to('ft').toNumber()]);
	TInfo.zb = await iterate(nbToIterate,0,[TInfo.zn,H.to('ft').toNumber()],"bisection",1/(12*100));//NOT PROPERLY CALCULATING!!!!!!!!!!!!!!!!!!---------------------------------
	let zb=TInfo.zb;
	console.log("zb: ",TInfo.zb);
	//finding fuel temp distribution
	let tco = function(z,mCh,qo,cp,hc,zn,zb){
			if(z<=zn){return tInf(z,mCh,qo,cp)+((heatFlux(z,qo).toNumber())/(hc.toNumber()));}//FORCED CONVECTION
			else{
				if(z>=zb){
					return Tsat.toNumber()+(1.897*Math.pow(heatFlux(z,qo).toNumber(),1/4)*Math.exp(-P.toNumber()/900));//THOM
				}else{
					//BERGELES AND ROSENHOW---------------------------------------------------------------------------------------------------------------------------------------
					return Tsat.toNumber()+(1.897*Math.pow(heatFlux(z,qo).toNumber(),1/4)*Math.exp(-P.toNumber()/900));//THOM
				}
			}
	};
	let tsurf = function(z,mCh,qo,cp,hc,zn,zb){
		return tco(z,mCh,qo,cp,hc,zn,zb)+(heatFlux(z,qo).toNumber()*(((Ro.toNumber())/(HG.toNumber()*(Ri.toNumber())))+(((Ro.toNumber())*Math.log(Ro.toNumber()/Ri.toNumber()))/(kc.toNumber()))));
	};
	let tfz = function(zz,m,q,c,h,znn,zbb){
		return function(to){
			let z=zz;let mCh=m;let qo=q;let cp=c;let hc=h;let zn=znn;let zb=zbb;
			let ts = tsurf(z,mCh,qo,cp,hc,zn,zb);
			return (3978.1*Math.log((692.61+ts)/(692.61+to)))+(Math.pow(10,0-12)*(Math.pow(ts+460,4)-Math.pow(to+460,4))*6.02366/4)+(Ri.toNumber()*heatFlux(z,qo).toNumber()/2);
		}
	};
	let records = [];
	let countConst = 12
	for(var i=0;i<H.toNumber()*countConst;i++){records.push([i/countConst,tInf(i/countConst,mCh,qo,cp)]);}
	for(var i=0;i<records.length;i++){
		records[i].push(Math.ceil(tco(records[i][0],mCh,qo,cp,hc,zn,zb)*1000)/1000);
		records[i].push(Math.ceil(await iterate(tfz(records[i][0],mCh,qo,cp,hc,zn,zb),0,findSigns(tfz(records[i][0],mCh,qo,cp,hc,zn,zb),[300,10000]),"bisection",1)*1000)/1000);
		records[i].push(enthalpy(records[i][0],mCh,qo).toNumber());
	}
	for(var i=0;i<records.length;i++){
		let newRec={
		"z":records[i][0],
		"zShort":Math.ceil(records[i][0]*100)/100,
		"T":{"inf":records[i][1],"clad":records[i][2],"centerline":records[i][3]}
		};
		records[i]=newRec;
	}
	let maxtemp1 = 0;
	for(var i=0;i<records.length;i++){
		if(records[i].T.centerline>maxtemp1){maxtemp1=records[i].T.centerline;}
	}
	//for(var i=0;i<records.length;i++){console.log([Math.ceil(records[i][0]*100)/100,records[i][1],records[i][2],records[i][3]]);}
	//let temprecords=[];//-------------------------------------------------------------------------------------------------
	//for(var i=0;i<records.length;i++){temprecords.push([records[i][1],records[i][2],records[i][3]]);}
	//console.log(records);//-----------------------------------------------------------------------------------------------
	if(!isFinal){return maxtemp1;}
	else{
		//--------------------------------------------------------------------------------------------
		let areaOf={};
		areaOf.chimney=Dchimney.toNumber()*Math.PI/4;
		areaOf.downcomer=(Dvessel.toNumber()*Math.PI/4)-areaOf.chimney;
		let massFlux={};
		massFlux.core=Gx.toNumber();
		let mCore=massFlux.core*Acore.toNumber();
		massFlux.chimney=massFlux.core/areaOf.chimney;
		massFlux.downcomer=massFlux.core/areaOf.downcomer;
		//get core losses
		let coreLosses = 0;
		let rhoBoil = 0;
		//acceleration
		let rhoInlet =rhoF.toNumber();
		let uin = TavgFluid.u.toNumber();
		let vin = (hin.toNumber()-TavgFluid.u.toNumber())/P.toNumber();
		let hExit = enthalpy(H.toNumber(),mCh,qo).toNumber();
		let xExit = quality(hExit);
		console.log("=======================================================================================================================");
		let findRhoOutlet = new Promise((accept,reject)=>{
				steamyPython.run([pythonScript,P.to('MPa').toNumber(),enthalpy(H.toNumber(),mCh,qo).to('kJ/kg').toNumber(),"PH"]).then((pyOut)=>{
					accept(new Quantity(pyOut[5]+" kg/m3").to(units.emp.rho).toNumber());
				},(pyErr)=>{reject(pyErr);});
		});
		let rhoOutlet = await findRhoOutlet;
		let voidExit = voidFrac(rhoOutlet); 
		coreLosses+=Math.pow(massFlux.core,2)*(((Math.pow(1-xExit,2)/((1-voidExit)*rhoF.toNumber()))+(Math.pow(xExit,2)/(voidExit*rhoG.toNumber())))-vin)/gc;
		//core local
		for(var i=0;i<nSpacers;i++){
			let local2Phase=1;
			if(zn<i*(H.toNumber()/7)){
				local2Phase=twoPhase(quality(enthalpy(i*(H.toNumber()/7),mCh,qo)));
			}
			coreLosses+=(Kgrid*(Math.pow(massFlux.core,2)/(2*gc*rhoF.toNumber()))*local2Phase);
		}
		coreLosses+=(KcoreIn*(Math.pow(massFlux.core,2)/(2*gc*rhoInlet)));
		coreLosses+=(KcoreOut*(Math.pow(massFlux.core,2)/(2*gc*rhoF.toNumber()))*twoPhase(quality(hExit),mCh,qo));console.log("Core nonfrictional losses: "+coreLosses);
		//core frictional
		let initZ = 0;
		if(zn>0){
			initZ=zn;
			let singlePhaseFriction =(Math.pow(massFlux.core,2)*initZ)/(De.to('ft').toNumber()*2*rhoF.toNumber()*gc);//THIS MUST BE MULTIPLIED BY THE FRICTION FACTOR!!!!!!!!!!!!!!!!-------------------------
		}
		let twoPhaseSum=0;
		for(var i=initZ*144;i<H.toNumber()*144;i++){
			twoPhaseSum+=twoPhase(quality(enthalpy(i/144,mCh,qo)))/12;
		}
		twoPhaseSum=twoPhaseSum/(H.toNumber()-initZ);
		let twoPhaseFricCore = Math.pow(massFlux.core,2)*(zn+((H.toNumber()-zn)*twoPhaseSum))/(De.to('ft').toNumber()*2*rhoF.toNumber()*gc);
		//NEED FRICTION FACTOR HERE!!!!!!!!!!!!!!!!!!!!!!--------------------------------------
		//get chimney losses
		let chimneyLosses =(Math.pow(massFlux.chimney,2)*Hchimney.toNumber()/(Dchimney.toNumber()*2*rhoF.toNumber()*gc))*twoPhase(quality(enthalpy(H.toNumber(),mCh,qo)));//THIS MUST BE MULTIPLIED BY THE FRICTION FACTOR!!!!!!!!!!!!!!!!-------------------------
		//get downcomer losses
		let downcomerLosses=0;
		//downcomer local
		downcomerLosses+=Kdowncomer*Math.pow(massFlux.downcomer,2)/(2*gc*rhoInlet);console.log("Downcomer nonfrictional losses: "+downcomerLosses);
		//downcomer friction
		
		let DeDownc = 4*areaOf.downcomer/(Math.PI*(Dbarrel.toNumber()+Dvessel.toNumber()));
		let downcFrict = (Math.pow(massFlux.downcomer,2)*Hdowncomer.toNumber()/(2*gc*rhoInlet*DeDownc));//THIS MUST BE MULTIPLIED BY THE FRICTION FACTOR!!!!!!!!!!!!!!!!-------------------------
		
		console.log("Total non-frictional losses: "+(downcomerLosses+coreLosses));
		console.log("Friction factors to find: ");
		let steel = 0.00015;let smooth=0.000005;
		console.log("Core: ","dp/f= "+twoPhaseFricCore,"Re= "+(massFlux.core*De.toNumber()/TavgFluid.mu.to('lb/ft hr').toNumber()),"e/D= "+(smooth/De.toNumber()));
		console.log("=======================================================================================================================");
		let muOut = TavgFluid.mu.toNumber();
		console.log("Chimney: ","dp/f= "+chimneyLosses,"Re= "+(massFlux.chimney*Dchimney.toNumber()/muOut),"e/D= "+(steel/Dchimney.toNumber()));
		console.log("=======================================================================================================================");
		let muIn = muF.toNumber();
		console.log("Downcomer: ","dp/f= "+downcFrict,"Re= "+(massFlux.downcomer*DeDownc/muIn),"e/D= "+(steel/DeDownc));
		return true;
		//Found frictional losses
		//core = 0.015 => 2548.75
		//chimney = 0.009 => 0.226
		//downcomer = 0.019 => 9.035
		//TOTAL = 2558.014
		//TOTAL LOSSES = 9647 lb/ft2 = 
	}
}

//_________________________________________________________________________________________________________________________________________________________________________________________________
//FUNCTIONS SPECIFIC TO FINDING MINIMUM MASS FLOW TO SATISFY QMELT REQUIREMENT



//-------------------------------------------------------------

//_________________________________________________________________________________________________________________________________________________________________________________________________
//CONSTRUCTOR DEFINITIONS

//Constructs an object containing all basic values regarding reactor thermal output
function Qcalc(thermalOutput){
	this.Q = thermalOutput; //Thermal output of the entire core
	this.qavg= this.Q.mul(gamma).div(H.mul(Do.div(2)).mul(2*Math.PI).mul(nAssemb*fuelRodsPerAssemb)).to('Btu/hr ft^2');//find q''avg
	this.qmax=  this.qavg.mul(Fq).to('Btu/hr ft^2');//max heat flux of the entire core AS WELL AS max heat flux of the hot channel //-----------------------------------------NO, SEE PG8 of 4.1 SOLN-----
	this.qohot=  this.qmax.div(qNorm(xMax)).to('Btu/hr ft^2');//heat flux equation modifier for the hot channel //-----------------------------------------NO, SEE PG8 of 4.1 SOLN-----
	this.qHotavg=  this.qmax.div(Fz).to('Btu/hr ft^2');//avg heat flux of the hot channel
	this.qavgMax= this.qavg.mul(Fz).to('Btu/hr ft^2');//max heat flux of the average channel
	this.qo=  this.qavgMax.div(qNorm(xMax)).to('Btu/hr ft^2');//heat flux equation modifier for the average channel
}

function thermoInfo(){
	//------------------------------------------------
}

//_________________________________________________________________________________________________________________________________________________________________________________________________
//_________________________________________________________________________________________________________________________________________________________________________________________________
//_________________________________________________________________________________________________________________________________________________________________________________________________
//RUNTIME LIFECYCLE



//Calculate some core dimensions
const Acore = CanDim.to('ft').pow(2).sub(Do.to('ft').pow(2).mul(Math.PI*fuelRodsPerAssemb/4)).mul(nAssemb).to('ft^2');//Core cross-sectional area //RECHECK--------------------------------
const Ax = S.to('ft').pow(2).sub(Do.to('ft').pow(2).mul(Math.PI).div(4)).to('ft^2');//Channel cross-sectional area //-------------------------
const De = Ax.to('ft2').mul(4).div(Do.to('ft').mul(Math.PI)).to('ft');//Equivalent Diameter for Channel //-----------------------------------------------
//Equivalent Diameter for Core Acore.mul(4).div(Do.mul(Math.PI)); IS NOT CORRECT!!!! WE NEED Pw!-------------------------------------------------------------------



//finding Extrapolation Length (lambda)
function qNorm(x){return (x*Math.sin(x));}//Simplified heat flux equation/qo
function dqdxL(x){return Math.sin(x)+(x*Math.cos(x));}//Equivalent sides of the simplified derivative of the heat flux equation/qo set to 0//================================================================
function dqdxR(x){return 0;}//Equivalent sides of the simplified derivative of the heat flux equation/qo set to 0//================================================================
iterate(dqdxL,dqdxR,approxMax(qNorm,[Math.PI,0]),"falsePosition",1/100).then((res)=>{
	xMax = res;
	function heTemp(x){return ((H.to('ft').toNumber())+(2*x));}//temporary function to calculate He given a guess at lambda
	//next line is EQN hw2.2.a ===================================================================================================================================================================================
	function lambdaFind(x){return H.to('ft').toNumber()*xMax*(Math.sin(xMax))/((x*Math.cos(Math.PI*x/heTemp(x)))-((heTemp(x)-x)*Math.cos(Math.PI*(heTemp(x)-x)/(heTemp(x))))-(((heTemp(x))/Math.PI)*(Math.sin(Math.PI*x/(heTemp(x)))-Math.sin(Math.PI*(heTemp(x)-x)/(heTemp(x))))));}
	function FzFunc(x){return Fz;}//Axial peaking factor
	function lamFzCombo(x){return (lambdaFind(x)-FzFunc(x));}//subtracting previous 2 equations from each other, should equal 0 is lambda is correct
	iterate(lambdaFind,FzFunc,findSigns(lamFzCombo,[0,(H.to('ft').toNumber()/2)]),"falsePosition",1/(12*100)).then((newLam)=>{
		lambda = new Quantity(newLam+' ft');//record found extrapolation distance
		He = H.add(lambda.mul(2)).to('ft');//record He now that we know extrapolation distance
		console.log("Extrapolation Distance (lambda): "+newLam+" ft");
		console.log("Fuel Height: "+H.to('ft').toNumber()+" ft");
		console.log("He: "+He.to('ft').toNumber()+" ft");
		//Create core instances now that we know extrapolation distance
		var core = [];//create array to hold cores of varying thermal outputs
		core.push(new Qcalc(Q));//generate object containing values regarding core thermal output
		core.push(new Qcalc(Q.mul(meltmax)));//generate object containing values regarding core thermal at the maximum allowable Qmax/Q=1.2, where TcenterlineMax MUST stay below Tmelt
		//nominal core is now core[0]
		//"melt" core is now core[1]
		//make qohot for core 1 =qo (workaround to re-use equations)
		core[0].qo=core[0].qohot;
		//FIND MASS FLUX SUCH THAT core[0] CPR is acceptable
		let GBounds = [1,999999999];//Core mass flux bounds to check within (kg/m2 s)<---------IMPORTANT!!!!
		let cprFunction = findCPRFunction(core[0]);
		iterate(cprFunction,0,[1000,4000],"bisection",0.0001).then((Gmetric)=>{//changed to 0.0001 accuracy because any further does not offer any significant difference
			let metricMassFlux = Math.ceil(Gmetric*1000)/1000;//round up to nearest 1000th of a kg/m2 s
			let CPRMinMassFlux = new Quantity(metricMassFlux+" kg/m2 s");//
			GBounds[0]=CPRMinMassFlux.to('kg/m2 s').toNumber();//Update mass flux boundaries (IN METRIC!!!) so we can iterate to find lowest mass flux that satisfies fuel centerline temp
			console.log("Minimum Mass Flux For CPR = "+CPRMinMassFlux.to('lb/ft2 hr').toNumber()+" lb/ft2 hr, == "+CPRMinMassFlux.to('kg/m2 s').toNumber());
			core[0].mCh=CPRMinMassFlux.to('lb/ft2 hr').mul(Ax.to('ft2')).to('lb/hr');
			core[0].mCore=CPRMinMassFlux.to('lb/ft2 hr').mul(Acore.to('ft2')).to('lb/hr');
			console.log("Minimum Channel Mass Flow For CPR = "+core[0].mCh.to('lb/hr').toNumber()+" lb/hr, == "+core[0].mCh.to('kg/s').toNumber()+" kg/s");
			console.log("Minimum Core Mass Flow For CPR = "+core[0].mCore.to('lb/hr').toNumber()+" lb/hr, == "+core[0].mCore.to('kg/s').toNumber()+" kg/s");
			console.log("hf="+hf.to(units.emp.h).toNumber()+" "+units.emp.h+", hg="+hg.to(units.emp.h).toNumber()+" "+units.emp.h);
			console.log("Max enthalpy of hot channel with min CPR = "+enthalpy(H,core[0].mCh,core[0].qohot).to(units.emp.h).toNumber()+" "+units.emp.h);
			findMaxFuelTemp(core[1],metricMassFlux,false).then((outputMax)=>{console.log("Maximum Fuel Centerline temperature with 1.2Q = "+outputMax);},(maxErr)=>{console.log(maxErr);});
			nominalInfo = findMaxFuelTemp(core[0],metricMassFlux,true);
			//DO INFO FOR NOMINAL CORE--------------------------------------------------------------------------------
		},(noMassFlowError)=>{console.log(noMassFlowError);});
		
		
		
		
		
		//FIND MASS FLOW RATE THAT SATISFIES core[1] MAX CENTERLINE Temp
		
		//CONFIRM THAT THE HIGHER MASSFLOW OF THE TWO SATISFIES BOTH REQUIREMENTS
		
		//
		
		
		
		
		
		
		
	},(lamErr)=>{
		console.log(lamErr);
	});
},(err)=>{console.log(err);});




